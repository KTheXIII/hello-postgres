# hello PostgreSQL

- [hello PostgreSQL](#hello-postgresql)
  - [How to use](#how-to-use)
    - [Start/Stop containers](#startstop-containers)
    - [Default values](#default-values)
    - [How To use pgAdmin](#how-to-use-pgadmin)
    - [Where is PostgreSQL](#where-is-postgresql)
  - [Docker tips](#docker-tips)

## How to use
You need docker installed.

### Start/Stop containers

Start containers in detached mode
```shell
docker-compose up -d
```

Stop containers
```shell
docker-compose down
```

### Default values

The default username for PostgreSQL and pgAdmin is

username: `user`

password: `password`

These can be change in the [docker-compose.yml](docker-compose.yml) file. The default created database is called `default`

For PostgreSQL it's under the `db` service at the `environment`.

```yml
    environment:
      - POSTGRES_DB=default
      - POSTGRES_USER=user
      - POSTGRES_PASSWORD=password
```

For pgAdmin it's under `pgadmin` service

```yml
    environment:
      - PGADMIN_DEFAULT_EMAIL=user
      - PGADMIN_DEFAULT_PASSWORD=password
```

### How To use pgAdmin

pgAdmin is running on port `3000`, to go [localhost:3000](http://localhost:3000) in the browser.

Login using the default `username` and `password`

Click on **Add New Server** in the **Quick Links**

Input a name.

Go to Connection, under *Hostname/address* use `pgdb` or your host computer hostname. Input the `username` and `passowrd` and press **save**.

### Where is PostgreSQL

PostgreSQL is on the port `5432` the default port.

## Docker tips

```shell
docker cp <file> <container-name>:/path/file
docker exec -it pgdb psql -U <user> -f /path/file
```

Copy over data to container
```shell
docker cp <filepath> <container-name>:<path-inside-container>
```

Run Postgres in the container
```shell
docker exec -it <container-name> psql --help 
```

Login into postgres with user
```shell
docker exec -it <container-name> psql -U <user>
```
